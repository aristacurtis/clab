#clab

# How To Use

## clone repository

```bash
$ git clone https://gitlab.com/aristacurtis/clab.git
```

### what it does

1.  This repo is meant to share and facilitate building network lab environments using containerlab.  

## dependencies

System requirements are a working containerlab installation, this is simply a collection of topology files, generated configurations, etc.
